import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import './ProductDetail.css';
import Comments from './Comments';

class ProductDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: {},
    }
  }

  componentDidMount() {
    let { id } = this.props.match.params;
    axios.get(`http://localhost:3333/api/v1/product/${id}`)
      .then(res => res.data.data[0])
      .then(data => {
        this.setState({
          data,
        })
      })
  }

  addToCart = () => {
    let { data } = this.state;
    let productCarts = []

    if (localStorage.getItem('product-cart')) {
      productCarts = JSON.parse(localStorage.getItem('product-cart'));
    }

    let isNew = true

    productCarts.forEach((item, index) => {
      if (item.Id === data.Id) {
        item.Quantity += 1;
        isNew = false
      }
    });

    if (isNew) {
      data.Quantity = 1;
      productCarts.push(data);
    }
    localStorage.setItem('product-cart', JSON.stringify(productCarts));
  }

  productIsExistInCart(id) {

  }

  render() {
    let { data } = this.state;
    return (
      <div className="container">
        <div className="card">
          <div className="container-fliud">
            <div className="wrapper row">
              <div className="preview col-md-6">

                <div className="preview-pic tab-content">
                  <div className="tab-pane active" id='pic-1'>
                    <img src={`http://localhost:3333/public/imgs/${data.Img_URL}`} alt="Product_Detail" />
                  </div>
                </div>

              </div>
              <div className="details col-md-6">
                <h3 className="product-title">{data.Name}</h3>
                <div className="rating">
                  <div className="stars">
                    <span className="fa fa-star checked"></span>
                    <span className="fa fa-star checked"></span>
                    <span className="fa fa-star checked"></span>
                    <span className="fa fa-star"></span>
                    <span className="fa fa-star"></span>
                  </div>
                </div>
                <p className="product-description">{data.Description}</p>
                <h4 className="price">current price:&nbsp;
                <span>${data.Price}</span>
                </h4>
                <h5 className="sizes">sizes:
                  <span className="size" data-toggle="tooltip" title="small">s</span>
                  <span className="size" data-toggle="tooltip" title="medium">m</span>
                  <span className="size" data-toggle="tooltip" title="large">l</span>
                  <span className="size" data-toggle="tooltip" title="xtra large">xl</span>
                </h5>
                <div className="action">
                  <Link className="btn-lg btn btn-success" onClick={this.addToCart} to="/cart">add to cart</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <div className="post-comments" id="comment-section"> */}
          <Comments product={data}/>
        {/* </div> */}
      </div>

    )
  }
}

export default ProductDetail;