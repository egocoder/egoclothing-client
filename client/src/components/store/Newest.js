import React from 'react';
import axios from 'axios';
import ProductItem from './ProductItem';
import ErrorPanel from '../app/ErrorPanel';

class Newest extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      errMessage: '',
      lstProduct: [],
    }
  }
  componentDidMount() {
    axios.get('http://localhost:3333/api/v1/product/newest')
    .then(res => res.data.data)
    .then(lstProduct => {
      this.setState({
        lstProduct
      })
    })
    .catch(err => {
      this.setState({
        errMessage: 'ERROR IN FETCH DATA'
      })
    })
  }

  render() {
    let lstProduct = this.state.lstProduct.map((product, index) => {
      let link = `detail/${product.Id}`
      return <ProductItem key={index} index={index} link={link} product={product} />
    })

    if (this.state.errMessage !== '') {
      return <ErrorPanel errMessage={this.state.errMessage} />
    }
    
    return (
      <React.Fragment>
        {lstProduct}
      </React.Fragment>
    )
  }
}

export default Newest;