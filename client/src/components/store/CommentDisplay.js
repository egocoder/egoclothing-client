import React from 'react';
import './CommentDisplay.css';
import axios from 'axios';

class CommentDisplay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: {},
        }
    }

    componentDidMount() {
        let tempComment = this.props.comment;
        axios.get(`http://localhost:3333/api/v1/account/${tempComment.Account_Id}`)
            .then(res => res.data.data[0])
            .then(Account => {
                this.setState({
                    comment: { ...tempComment, Account }
                })
            })
    }

    render() {
        let comment = this.state.comment;
        return (
            <div className="row">
                <div className="col-sm-2">
                    <div className="thumbnail">
                        <img className="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png" alt="Image_User_Comment" />
                    </div>
                </div>
                <div className="col-sm-10">
                    <div className="panel panel-info">
                        <div className="panel-heading">
                            <strong>{(comment.Account) ? comment.Account.Name : ""}</strong>
                            <span className="text-muted"> commented 5 days ago</span>
                        </div>
                        <div className="panel-body">
                            <p>{comment.Content}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CommentDisplay;