import React from 'react';
import './PagingButton.css';

class PagingButton extends React.Component {
  onPaginClick = () => {
    this.props.onPaginClick(this.props.index);
  }
  render() {
    return (
      <button className={'btn btn-lg ' + this.props.isSelected} onClick={this.onPaginClick}>{this.props.index + 1}</button>
    )
  }
}

export default PagingButton;