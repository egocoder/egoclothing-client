import React from 'react';
import axios from 'axios';
import jwtDecode from 'jwt-decode';
import CommentDisplay from './CommentDisplay';
import CommentForm from './CommentForm'

class Comments extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoaded: false,
      isLogined: false,
      Product_Id: 0,
    }
  }

  componentDidMount() {
    let token = JSON.parse(localStorage.getItem('user-token'));
    if (token) {
      this.setState({
        isLogined: true
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    let { product } = nextProps;
    // console.log(data);
    axios.get(`http://localhost:3333/api/v1/comment/byproduct/${product.Id}`)
      .then(res => res.data.data)
      .then(data => {
        this.setState({
          data,
          isLoaded: true,
          Product_Id: product.Id
        })
      })
  }

  onAddedComment = (content) => {
    let token = JSON.parse(localStorage.getItem('user-token'));
    if (!token) {
      return
    }
    let Account = jwtDecode(token);

    let newComment = {
      Comment_Id: -1,
      Content: content,
      Account_Id: Account.user.id,
      Product_Id: this.state.Product_Id
    }

    axios.post(`http://localhost:3333/api/v1/comment/`, newComment)
      .then(res => {
        newComment = {
          ...newComment, Account: {
            Name: Account.user.username
          }
        }
        this.setState({
          data: [...this.state.data, newComment]
        })
      })
      .catch(err => {
        console.log(`Error in CREATE API - ${err}`);
      })
  }

  render() {
    let Comments = this.state.data;
    let lstComment = Comments.map((comment, index) => {
      return <CommentDisplay comment={comment} key={index} />
    })
    return (

      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h3>User Comments</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            {lstComment}
          </div>
          <div class="col-sm-6">
            {(this.state.isLogined) ? <CommentForm onAddedComment={this.onAddedComment} /> :
              <a href="/account/signin" className="btn btn-danger">Please Login to comment</a>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default Comments;