import React from 'react';
import { Link } from 'react-router-dom';
import './ProductItem.css'

class ProductItem extends React.Component {


  onOrderProduct = (event) => {
    let { product } = this.props;
    let productCarts = []

    if (localStorage.getItem('product-cart')) {
      productCarts = JSON.parse(localStorage.getItem('product-cart'));
    }

    let isNew = true

    productCarts.forEach((item, index) => {
      if (item.Id === product.Id) {
        item.Quantity += 1;
        isNew = false
      }
    });

    if (isNew) {
      product.Quantity = 1;
      productCarts.push(product);
    }
    localStorage.setItem('product-cart', JSON.stringify(productCarts));
  }

  render() {
    // console.log(this.props.location);
    var { product, link } = this.props;
    // let link = `detail/${product.MaSanPham}`
    return (
      <div className="col-xs-12 col-sm-6 col-md-6 col-lg-4">
        <div className="shop-card">
          <div className="title">{product.Name}</div>
          <div className="slider">
            <img src={`http://localhost:3333/public/imgs/${product.Img_URL}`} alt="Product_Image" />
          </div>

          <div className="cta">
            <div className="price">$ {product.Price}</div>
            <Link className="btn" role="button" to={link}>View Detail</Link>
            <button onClick={this.onOrderProduct} className="btn">Add To Cart</button>
          </div>
        </div>
      </div>
    )
  }
}

export default ProductItem;


