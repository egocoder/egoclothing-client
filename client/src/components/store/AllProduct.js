import React from 'react';
import axios from 'axios';
import ProductItem from './ProductItem';
import PagingButton from './PagingButton';
import ErrorPanel from '../app/ErrorPanel';
import './AllProduct.css';

class AllProduct extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      errMessage: '',
      lstProduct: [],
      totalPage: 0,
      paginButtonSelected: 0
    }
  }
  componentDidMount() {
    axios.get('http://localhost:3333/api/v1/product')
      .then(res => res.data.data)
      .then(data => {
        let modTotalPage = data.length % 6 !== 0 ? 1 : 0
        this.setState({
          lstProduct: data.slice(0, 6),
          totalPage: parseInt(data.length / 6, 10) + modTotalPage
        })
      })
      .catch(err => {
        this.setState({
          errMessage: 'ERROR IN FETCH DATA'
        })
      })
  }
  onPaginClick = (index) => {
    axios.get(`http://localhost:3333/api/v1/product?limit=6&offset=${index * 6}`)
      .then(res => res.data.data)
      .then(lstProduct => {
        this.setState({
          lstProduct,
          paginButtonSelected: index
        });
      })
      .catch(err => {
        this.setState({
          errMessage: 'ERROR IN FETCH DATA'
        })
      })
  }
  render() {
    let lstProduct = this.state.lstProduct.map((product, index) => {
      let link = `store/detail/${product.Id}`
      return <ProductItem key={index} index={index} link={link} product={product} />
    })
    let lstPagin = Array(this.state.totalPage).fill(0).map((value, index) => {
      return <PagingButton key={index} isSelected={this.state.paginButtonSelected === index ? 'btn-primary' : 'btn-default'} index={index} onPaginClick={this.onPaginClick} />
    })

    if (this.state.errMessage !== '') {
      return <ErrorPanel errMessage={this.state.errMessage} />
    }

    return (
      <React.Fragment>
        {lstProduct}
        <div className="paging-btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
          <div className="btn-group">
            {lstPagin}
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default AllProduct;