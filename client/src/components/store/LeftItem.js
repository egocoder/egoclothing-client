import React from 'react';
import {Link} from 'react-router-dom';
import './LeftItem.css'

class LeftItem extends React.Component {
  
  onLeftItemClick = () => {
    this.props.onLeftItemClick(this.props.index);
  }

  render() {
    let {pathname} = this.props.location;
    let { data } = this.props;
    return (
      <li className={data.path === pathname ? 'active' : ''} ><Link to={data.path}>{data.name}</Link></li>
    )
  }
}

export default LeftItem;