import React from 'react';
import {Switch, Route} from 'react-router-dom';
import AllProduct from './AllProduct';
import Newest from './Newest';
import Feature from './Feature';

class RightControl extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path='/store/newest' component={Newest}/>
        <Route exact path='/store/feature' component={Feature}/>
        <Route exact path='/store' component={AllProduct}/>
      </Switch>
    )
  }
}

export default RightControl;