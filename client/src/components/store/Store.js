import React from 'react';
import Panel from '../app/Panel';
import LeftItem from './LeftItem';
import RightControl from './RightControl';

class Store extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      leftDatas: [
        {
          path: '/store/newest',
          name: 'Newest',
        },
        {
          path: '/store/feature',
          name: 'Feature',
        },
        {
          path: '/store',
          name: 'All Product',
        }
      ]
    }
  }

  render() {
    let lstLeftItem = this.state.leftDatas.map((data, index) => {
      return <LeftItem key={index} index={index} data={data} {...this.props} />
    })
    return (
      <React.Fragment>
        <Panel />
        <div className="container-fluid">
          <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <ul className="nav nav-pills nav-stacked">
              {lstLeftItem}
            </ul>
          </div>
          <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <RightControl />
          </div>
        </div>
      </React.Fragment>

    )
  }
}

export default Store;