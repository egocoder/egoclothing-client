import React from 'react';
import './CommentForm.css';

class CommentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Content: "",
        }
      }
    
    PostComment = () =>{
        let Content = this.state.Content;
        if(Content === "")
        {
            alert("Please write your comment!");
            return;
        }
        else{
            this.props.onAddedComment(this.state.Content);
            this.setState({
                Content: ""
            })
        }
    }

    onValueChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
    
        this.setState({
          [name]: value
        })
      }

  render() {
      return(
        <div className="row">
          <div className="col-lg-12">
            <textarea
                id="comment-content"
                name="Content"
                className="textarea-comment"
                value={this.state.Content} 
                onChange={ this.onValueChange }>
            </textarea>
            <div className="comment-post">
                <button id="comment-btn-post" className="btn btn-primary" onClick={this.PostComment}>Post</button>
            </div>
          </div>
        </div>
      );
  }
}

export default CommentForm;