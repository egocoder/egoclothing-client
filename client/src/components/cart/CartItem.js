import React from 'react';

class CartItem extends React.Component {
  onValueChange = (event) => {
    
    let target = event.target;
    // let name = target.name;
    let value = target.value;

    let cartEditing = {
      index: this.props.index,
      quantity: value
    }

    this.props.onQuantityChange(cartEditing);
  }

  onDeleteCart = () => {
    this.props.onDeleteCart(this.props.index);
  }


  render() {
    let { cart } = this.props;
    return (
      <tr>
        <td>
          <img className="cart-item-image" src={`http://localhost:3333/public/imgs/${cart.Img_URL}`} alt="CartItem_Image" />
        </td>
        <td className="cart-item-name">{cart.Name}</td>
        <td>
          <input className="form-control" type="number" name="quantity" value={cart.Quantity} max="100" min="0" onChange={this.onValueChange} />
        </td>
        <td className="text-right">$ {cart.Price}</td>
        <td className="text-right">
          <button onClick={this.onDeleteCart} className="btn btn-sm btn-danger">
            <i className="fa fa-trash"></i>
          </button>
        </td>
      </tr>
    )
  }
}

export default CartItem;