import React from 'react';
import CartItem from './CartItem';
import { Link } from 'react-router-dom';
import './Cart.css';
import jwtDecode from 'jwt-decode';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lstCart: [],
      totalPrice: 0,
      statusCode: 201,
      isEmptyCart: false,
      name: '',
      address: '',
      phoneNumber: '',
      email: ''
    }
  }

  componentDidMount() {

    let lstCart = JSON.parse(localStorage.getItem('product-cart'));
    if (lstCart) {
      let totalPrice = this.sumOfPrice(lstCart);
      if (lstCart) {
        this.setState({
          lstCart,
          totalPrice
        })
      }
    }

  }

  onDeleteCart = (index) => {
    let { lstCart } = this.state;
    lstCart.pop(lstCart[index]);
    let totalPrice = this.sumOfPrice(lstCart);
    this.setState({
      lstCart,
      totalPrice
    })
    localStorage.setItem('product-cart', JSON.stringify(lstCart));
  }

  findCart(index) {
    let cart = null;
    let { lstCart } = this.state;
    lstCart.forEach((item, i) => {
      if (index === i) {
        cart = item
      }
    })
    return cart;
  }

  sumOfPrice(lstCart) {
    let s = 0;
    lstCart.forEach(cart => {
      s += (parseFloat(cart.Price) * parseInt(cart.Quantity, 10));
    })
    return s;
  }

  onValueChange = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.value;

    this.setState({
      [name]: value
    })
  }

  isAcceptOrder = () => {
    let { name, address, phoneNumber, email, totalPrice } = this.state;
    let flag = false;
    if (name !== '' && address !== '' && phoneNumber !== '' && email !== '' && totalPrice > 0) {
      flag = true;
    }

    return flag;
  }

  onOrderProduct = (event) => {
    let token = JSON.parse(localStorage.getItem('user-token'));
    if (!token) {
      this.setState({
        statusCode: 403
      })
      return
    }
    let { user } = jwtDecode(token);

    if (!this.isAcceptOrder()) {
      return
    }

    // SEND MAIL

    let { receiveMailers } = this.state;
    axios.post('http://localhost:3333/api/v1/business/subscribeEmail', { receiveMailers })
    .then(res => res.data)
    .then(data => {
      alert("your mail has sent");
      this.setState({
        receiveMailers: ''
      })
    })
    .catch(err => {
      console.log(err);
    })

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.post(`http://localhost:3333/api/v1/order/${user.username}`, {
      Total: this.state.totalPrice
    })
      .then(res => res.data)
      .then(data => {
        if (data.status > 201) {
          this.setState({
            statusCode: 403
          })
          return
        }
        this.setState({
          isEmptyCart: true
        })
        localStorage.removeItem('product-cart')
      })
      .catch(err => {
        this.setState({
          statusCode: 403
        })
      })
  }

  onQuantityChange = (cartEditing) => {
    let { lstCart } = this.state;
    lstCart.forEach((cart, index) => {
      if (cartEditing.index === index) {
        cart.Quantity = cartEditing.quantity
      }
    })
    let totalPrice = this.sumOfPrice(lstCart);
    this.setState({
      lstCart,
      totalPrice
    })
    localStorage.setItem('product-cart', JSON.stringify(lstCart));
  }

  render() {

    if (this.state.statusCode > 201) {
      return <Redirect to="account/signin" />
    }

    let lstCart = this.state.lstCart.map((cart, index) => {
      return <CartItem key={index} cart={cart} onQuantityChange={this.onQuantityChange} onDeleteCart={this.onDeleteCart} index={index} />
    })

    if (this.state.isEmptyCart) {
      return <Redirect to="account" />
    }


    return (

      <div className="container mb-4">
        <h1>{this.state.errMessage}</h1>
        <div className="row">
          <div className="col-12">
            <div className="table-responsive">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th scope="col"> </th>
                    <th scope="col">Product</th>
                    <th scope="col" className="text-center">Quantity</th>
                    <th scope="col" className="text-right">Price</th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody>
                  {lstCart}
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                      <strong>Total</strong>
                    </td>
                    <td className="text-right">
                      <strong>$ {this.state.totalPrice}</strong>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <form id="order-information" className="form">
            <h3>Please fill in this form</h3>
            <div className="form-group">
              <label>Full Name</label>
              <input type="name"
                required="required"
                name="name"
                value={this.state.name}
                onChange={this.onValueChange}
                className="form-control" />
            </div>
            <div className="form-group">
              <label>Address</label>
              <input type="name"
                required="required"
                name="address"
                value={this.state.address}
                onChange={this.onValueChange}
                className="form-control" />
            </div>
            <div className="form-group">
              <label>Phone Number</label>
              <input type="name"
                required="required"
                name="phoneNumber"
                value={this.state.phoneNumber}
                onChange={this.onValueChange}
                className="form-control" />
            </div>
            <div className="form-group">
              <label>Email</label>
              <input type="email"
                required="required"
                name="email"
                value={this.state.email}
                onChange={this.onValueChange}
                className="form-control" />
            </div>
          </form>
          <div className="cart-footer">
            <Link className="btn btn-lg  btn-continue text-uppercase" to="/store"><i className="fa fa-angle-left" /><i className="fa fa-angle-left" />&nbsp;continue shop</Link>
            <button onClick={this.onOrderProduct} className="btn btn-lg btn-checkout text-uppercase" to="/store">checkout&nbsp;<i className="fa fa-angle-right" /><i className="fa fa-angle-right" /></button>
          </div>
        </div>
      </div >
    )
  }
}

export default Cart;