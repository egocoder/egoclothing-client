import React from 'react';
import './Menu.css';
import './Admin.css';
import MenuBarItem from './MenuBarItem';
import Category from './Category';
import Brand from './Brand';
import Account from './Account';
import Product from './Product';
import Order from './Order';

class Admin extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        itemSelected:  {
          content: 'Account',
          isActive: true
        },
        lstMenuBarItem: [
          {
            content: 'Account',
            isActive: true
          },
          {
            content: 'Product',
            isActive: false
          },
          {
            content: 'Category',
            isActive: false
          },
          {
            content: 'Brand',
            isActive: false
          },
          {
            content: 'Order',
            isActive: false
          }
        ],
        // lstProduct
        lstProduct: [],
        // lstAccount
        lstAccount: [],
        // lstCategory
        lstCategory: [],
        // lstBrand
        lstBrand: [],
        // lst Order
        lstOrder: [],
      }
    }

    onMenuBarClick = (index) => {
        let { lstMenuBarItem } = this.state;
        let itemSelected = this.findMenuBarItem(index);
        lstMenuBarItem.forEach((item) => {
          item.isActive = false
        })
        lstMenuBarItem[index].isActive = !lstMenuBarItem[index].isActive
        this.setState({
          itemSelected,
          lstMenuBarItem
        })
      }
    
      findMenuBarItem = (index) => {
        let { lstMenuBarItem } = this.state;
        let rs = null;
        lstMenuBarItem.forEach((item, i) => {
          if (i === index) {
            rs = item
          }
        })
        return rs;
      }
  
    renderContent = () => {
    
      let {itemSelected} = this.state
      
      switch (itemSelected.content) {
        case 'Product':
          return <Product/>;
        case 'Category':
          return <Category />;
        case 'Brand':
          return <Brand />;
        case 'Order':
          return <Order />;
        default:
          return <Account/>
      }
    }
  
  
    render() {
      let lstMenuBarItem = this.state.lstMenuBarItem.map((item, index) => {
        return <MenuBarItem Content={item.content} key={index} index={index} 
        isActive={item.isActive} onMenuBarItemClick={this.onMenuBarClick}/>
      })
      return (
        <div className="container-fluid admin-pills">
          <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
            <ul className="nav nav-pills nav-stacked">
              {lstMenuBarItem}
            </ul>
          </div>
          <div className="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            {this.renderContent()}
          </div>
        </div>
      )
    }
  }
  
  export default Admin;