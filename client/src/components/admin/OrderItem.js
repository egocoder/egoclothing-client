import React from 'react';

class OrderItem extends React.Component {

  renameStatus = (status) => {
    let rs = '';
    switch (status) {
      case 2:
        rs = "Delivering"
        break;
      case 3:
        rs = "Paid"
        break;
      case 4:
        rs = "Cancel"
        break;
      default:
        rs = "Ordered"
        break;
    }
    return rs;
  }

  fillModalForm = () =>{
    this.props.onToggleEditForm(this.props.order.Order_Id);
  }

  render() {
    let { order } = this.props;
    let orderStatus = this.renameStatus(order.Status_Id);
    
    return (
      <tr>
        <td className="cart-item-name">{order.Order_Id}</td>
        <td>{order.Date}</td>
        <td>{orderStatus}</td>
        <td>$ {order.Total}</td>
        <td><button className="btn btn-success" onClick={this.fillModalForm}>Detail</button></td>
      </tr>
    );
  }
}

export default OrderItem;