import React, { Component } from 'react';
import axios from 'axios';
import Thumbnail from './Thumbnail';
import BrandForm from './BrandForm';

class Brand extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errMessage: '',
      isLoaded: false,
      brands: [],
      curBrand: null,
      isOpenModal: false,
      btnStyle: 'fa fa-plus',
    };
  }

  onToggleAddNewForm = () => {
    this.setState({
      isOpenModal: !this.state.isOpenModal,
      btnStyle: (this.state.btnStyle === 'fa fa-plus') ? 'fa fa-minus' : 'fa fa-plus',
    })
  }

  onAddNew = (newBrand) => {
    let newBrands = [...this.state.brands, newBrand];
    this.setState({ brands: newBrands });
  }

  onDelete = (index) => {
    let items = [...this.state.brands];
    let i = items.findIndex(cur => cur.Brand_Id === index);
    items.splice(i, 1);
    this.setState({
      brands: items
    })
  }

  // Update 17/05

  onFormClear = () => {
    this.setState({
      curBrand: null,
    })
  }


  getBrand = (index) => {
    let token = JSON.parse(localStorage.getItem('user-token'));
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get(`http://localhost:3333/api/v1/brand/${index}`)
      .then(res => res.data)
      .then(data => {
        this.setState({
          curBrand: data.data[0]
        });
      })
      .catch(err => {
        this.setState({
          isLoaded: true,
          errMessage: err
        })
      })
  }

  onUpdateHandled = (index) => {
    this.getBrand(index);

    // Open form
    if (!this.state.isOpenModal) {
      this.onToggleAddNewForm();
    }
  }

  onUpdated = (data) => {
    // Copy array
    let arr = this.state.brands.slice();
    let i = this.state.brands.findIndex((cur) => cur.Brand_Id === data.Brand_Id);
    if (i >= 0) {
      arr[i] = data;
      this.setState({
        brands: arr,
      })
      this.onToggleAddNewForm();
    }
  }


  componentDidMount() {
    let token = JSON.parse(localStorage.getItem('user-token'));
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get("http://localhost:3333/api/v1/brand")
      .then(res => res.data)
      .then(data => {
        if (data.status > 201) {
          this.setState({
            errMessage: data.message
          })
          return
        }
        this.setState({
          isLoaded: true,
          brands: data.data
        });
      })
      .catch(err => {
        this.setState({
          isLoaded: true,
          errMessage: err
        });
      })
  }

  render() {
    let listThumbnail = this.state.brands.map((Brand) => {
      return <Thumbnail key={Brand.Brand_Id}
        index={Brand.Brand_Id}
        name={Brand.Brand_Name}
        onDelete={this.onDelete}
        onUpdate={this.onUpdateHandled}
        image_url={Brand.LogoURL}
        type={4}
      />
    })
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="container-fluid">
          <div className="jumbotron">
            <h1>brands</h1>
          </div>
          <hr />
          <button type="button"
            className="btn btn-lg btn-success"
            onClick={this.onToggleAddNewForm}
          ><i className={this.state.btnStyle}></i></button>
          <hr />
          <BrandForm isOpenModal={this.state.isOpenModal}
            onAddNew={this.onAddNew}
            onToggleAddNewForm={this.onToggleAddNewForm}
            onFormClear={this.onFormClear}
            onUpdated={this.onUpdated}
            Brand={this.state.curBrand}
          />
          <div className="row">
            {listThumbnail}
          </div>
        </div>
      );
    }
  }
}

export default Brand;