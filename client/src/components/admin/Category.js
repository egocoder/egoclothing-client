import React, { Component } from 'react';
import axios from 'axios';
import Thumbnail from './Thumbnail';
import CategoryForm from './CategoryForm';

class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errMessage: '',
      isLoaded: false,
      categories: [],
      curCategory: null,
      isOpenModal: false,
      btnStyle: 'fa fa-plus',
    };
  }

  onToggleAddNewForm = () => {
    this.setState({
      isOpenModal: !this.state.isOpenModal,
      btnStyle: (this.state.btnStyle === 'fa fa-plus') ? 'fa fa-minus' : 'fa fa-plus',
    })
  }

  onAddNew = (data) => {
    // alert(data);
    let newCategories = [...this.state.categories, data]
    this.setState({ categories: newCategories });
  }

  onDelete = (index) => {
    let items = [...this.state.categories];
    let i = items.findIndex(cur => cur.Category_Id === index);
    items.splice(i, 1);
    this.setState({
      categories: items
    })
  }

  onFormClear = () => {
    this.setState({
      curCategory: null,
    })
  }


  getCategory = (index) => {
    let token = JSON.parse(localStorage.getItem('user-token'));
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get(`http://localhost:3333/api/v1/category/${index}`)
      .then(res => res.data)
      .then(data => {
        this.setState({
          curCategory: data.data[0]
        });
      })
      .catch(err => {
        this.setState({
          isLoaded: true,
          errMessage: err
        })
      })
  }

  onUpdateHandled = (index) => {
    this.getCategory(index);
    // Open form
    if (!this.state.isOpenModal) {
      this.onToggleAddNewForm();
    }
  }

  onUpdated = (data) => {
    // Copy array
    let arr = this.state.categories.slice();
    let i = this.state.categories.findIndex((cur) => cur.Category_Id === data.Category_Id);
    if (i >= 0) {
      arr[i] = data;
      this.setState({
        categories: arr,
      })
      this.onToggleAddNewForm();
    }
  }


  componentDidMount() {
    let token = JSON.parse(localStorage.getItem('user-token'));
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get("http://localhost:3333/api/v1/category")
      .then(res => res.data)
      .then(data => {
        if (data.status > 201) {
          this.setState({
            errMessage: data.message
          })
          return
        }
        this.setState({
          isLoaded: true,
          categories: data.data
        });
      })
      .catch(err => {
        this.setState({
          isLoaded: true,
          errMessage: err
        });
      })
  }

  render() {
    let listThumbnail = this.state.categories.map((category) => {
      return <Thumbnail key={category.Category_Id}
        index={category.Category_Id}
        name={category.Category_Name}
        onDelete={this.onDelete}
        onUpdate={this.onUpdateHandled}
        image_url=""
        type={3}
      />
    })
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="container-fluid">
          <div className="jumbotron">
            <h1>Categories</h1>
          </div>
          <hr />
          <button type="button"
            className="btn btn-lg btn-success"
            onClick={this.onToggleAddNewForm}
          ><i className={this.state.btnStyle}></i></button>
          <hr />
          <CategoryForm isOpenModal={this.state.isOpenModal}
            onAddNew={this.onAddNew}
            onToggleAddNewForm={this.onToggleAddNewForm}
            onFormClear={this.onFormClear}
            onUpdated={this.onUpdated}
            Category={this.state.curCategory}
          />
          <div className="row">
            {listThumbnail}
          </div>
        </div>
      );
    }
  }
}

export default Category;