import React from 'react';
import axios from 'axios';

class BrandForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Brand_Name: '',
      LogoURL: '',
      isNew: true,
      Brand_Id: -1,
      Title: 'Add New Brand',
    }
  }

  componentDidUpdate() {
    let Brand = this.props.Brand;

    if (Brand !== null && this.state.Brand_Id !== Brand.Brand_Id) {
      this.setState({
        Brand_Name: Brand.Brand_Name,
        Brand_Id: Brand.Brand_Id,
        LogoURL: Brand.LogoURL,
        isNew: false,
        Title: 'Edit Brand',
      })
    }
  }

  onClear = () => {
    this.props.onFormClear();

    this.setState({
      Brand_Name: '',
      LogoURL: '',
      Brand_Id: -1,
      isNew: true,
      Title: 'Add New Brand',
    })
  }

  uploadImage = () => {
    return new Promise((resolve, reject) => {
      const form = new FormData(this.form);
      let uploadImage = form.get('uploadImage');
 
      const imgData = new FormData();
      imgData.append('file', uploadImage);
      imgData.append('filename', (uploadImage.name).slice(0, (uploadImage.name).length - 4));

      fetch('http://localhost:3333/api/v1/business/uploadImage', { method: 'POST', body: imgData })
        .then(res => res.json())
        .then(body => {
          resolve(body)
        })
        .catch(err => {
          reject(err);
        })
    })
  }

  onSubmit = (event) => {
    event.preventDefault();

    this.uploadImage()
    .then(body => {
      if (this.state.isNew === true) {
        let brand = this.state;
        axios.post(`http://localhost:3333/api/v1/brand`, brand)
          .then(res => res.data)
          .then(data => {
            let newBrand = { ...brand, Brand_Id: data.data.insertId }
            this.props.onAddNew(newBrand);

          })
          .catch(err => {
            console.log(`Error in CREATE API - ${err}`);
          })
        this.props.onToggleAddNewForm();
        this.onClear();
      }
      // Edit event
      else {
        let brand = {
          Brand_Id: this.state.Brand_Id,
          Brand_Name: this.state.Brand_Name,
          LogoURL: this.state.LogoURL
        };
        axios.put(`http://localhost:3333/api/v1/brand/` + brand.Brand_Id, brand)
          .then(res => {
            this.props.onUpdated(brand);
          })
          .catch(err => {
            console.log(`Error in CREATE API - ${err}`);
          })
      }
    })
  }

  onValueChange = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.value;

    // bind LogoURL
    if (name === 'uploadImage') {
      let imageName = value.slice(12, value.length - 3) + 'jpg';
      name = 'LogoURL';
      value = `http://localhost:3333/public/imgs/${imageName}`
    }

    this.setState({
      [name]: value
    });
  }

  render() {
    let className = this.props.isOpenModal ? 'panel-success openModel' : 'panel-success closeModel';

    return (
      <div className={className}>
        <div className="panel-heading">
          <h3 className="panel-title">{this.state.Title}</h3>
        </div>
        <div className="panel-body">
          <form onSubmit={this.onSubmit} ref={el => (this.form = el)}>
            <div className="form-group">
              <label>Tên Hãng Sản Xuất:</label>
              <input
                type="text"
                className="form-control"
                name="Brand_Name"
                value={this.state.Brand_Name}
                onChange={this.onValueChange}
              />
            </div>
            <div className="form-group">
              <label>Logo URL:</label>
              <input
                type="text"
                className="form-control"
                name="LogoURL"
                value={this.state.LogoURL}
                onChange={this.onValueChange}
              />
            </div>
            <div className="form-group">
              <label>Image:</label>
              <input
                type="file"
                name="uploadImage"
                className="form-control"
                onChange={this.onValueChange}
              />
            </div>
            <br />
            <div className="text-center">
              <button
                type="submit"
                className="btn btn-success btn-lg"
              >Save</button>&nbsp;
                  <button
                type="button"
                className="btn btn-danger btn-lg"
                onClick={this.onClear}
              >Clear</button>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

export default BrandForm;