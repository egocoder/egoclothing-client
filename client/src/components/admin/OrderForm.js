import React from 'react';
import axios from 'axios';
import './Form.css';
import './OrderForm.css';

class CategoryForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Order_Id: -1,
      Name: '',
      PhoneNumber: '',
      Address: '',
      Email: '',
      Status_Id: -1,
      Title: 'Detail Order',
      Statuses: [],
    }
  }

  componentDidMount(){
    this.LoadStatuses();
  }

  LoadStatuses = () => {
    axios.get('http://localhost:3333/api/v1/status')
      .then(res => res.data)
      .then(data => {
        if (data.status > 201) {
          this.setState({
            errMessage: data.message
          })
          return
        }
        this.setState({
            Statuses: data.data,
        });
      })
  }
  
  componentDidUpdate(){
    let order = this.props.order;
    if(order !== null && this.state.Order_Id !== order.Order_Id)
      this.setState({
        Name: order.Name,
        Address: order.Address,
        PhoneNumber: order.PhoneNumber,
        Email: order.Email,
        Order_Id: order.Order_Id,
        Status_Id: order.Status_Id,
      })
  }

  onValueChange = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.value;

    this.setState({
      [name]: value
    })
  }

  onChangeStatus = (id) =>{
        if(this.state.Status_Id === id)
            return;
        else{
            let data = {
                Order_Id: this.state.Order_Id,
                Status_Id: id
            }
            axios.put('http://localhost:3333/api/v1/order/updatestatus/'+this.state.Order_Id,data)
            .then(res => res.data)
            .then(data => {
                if (data.status > 201) {
                this.setState({
                    errMessage: data.message
                })
                return
                }
                this.setState({
                    Status_Id: id,
                });
                this.props.onStatusUpdated(this.state.Status_Id);
            })
        }
  }

  render() {
    let className = this.props.isOpenModal ? 'panel-success openModel' : 'panel-success closeModel';
    let renderBtnStatus = this.state.Statuses.map((status, index) => {
        let statusStyle = '';
        switch(status.Id){
            case 2: statusStyle = 'btn btn-primary';break;
            case 3: statusStyle = 'btn btn-success';break;
            case 4: statusStyle = 'btn btn-danger';break;
            default: statusStyle = 'btn btn-warning';break;
        }
        if(status.Id === this.state.Status_Id) statusStyle += ' disabled';
        return <button  type="button"
                        className={statusStyle}
                        onClick={() => {this.onChangeStatus(status.Id)}}
                        key={status.Id}>{status.Name}</button>
      })
    return (
      <div className={className}>
        <div className="panel-heading">
          <h3 className="panel-title">{this.state.Title}</h3>
        </div>
        <div className="panel-body">
          <div className="form">
            <div className="form-group">
                <h3>Customer Name</h3>
                <div>{this.state.Name}</div>
            </div>
            <div className="form-group">
                <h3>Customer Address</h3>
                <div>{this.state.Address}</div>
            </div>
            <div className="form-group">
                <h3>Customer PhoneNumber</h3>
                <div>{this.state.PhoneNumber}</div>
            </div>
            <div className="form-group">
                <h3 >Customer Email</h3>
                <div>{this.state.Email}</div>
            </div>
            {renderBtnStatus}
          </div>
        </div>
      </div>

    )
  }
}

export default CategoryForm;