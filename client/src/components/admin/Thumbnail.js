import React from 'react';
import axios from 'axios';
import './Thumb.css';
import { Redirect } from 'react-router-dom';

class Thumbnail extends React.Component {

  update = (event) => {
    let { index } = this.props;  
    this.props.onUpdate(index);
  }
  
  delete = (event) => {
    let token = JSON.parse(localStorage.getItem('user-token'));
    if (!token) {
      return <Redirect to='account/signin'/>
    }
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    let { index } = this.props;
    
    let url = 'http://localhost:3333/api/v1/';
    switch(this.props.type){
      case 1: url += 'account';break;
      case 2: url += 'product';break;
      case 3: url += 'category';break;
      case 4: url += 'brand';break;
      default: break;
    }
    url += '/' + index;
    
    event.preventDefault();
    axios.delete(url)
    .then(res => {
      this.props.onDelete(index);
    })
  }

  render() {
    return (
      <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div className="thumbnail">
          <div className="caption">
              <img className="center-block" width="200" height="200" src={this.props.image_url} alt='Thumbnails_Image'/>
              <h3>{this.props.name}</h3>
              <p className="fullname">{this.props.description}</p>
            <p>
              <button type="button"
                      className="btn btn-warning"
                      onClick={this.update}
                      ><i className="fa fa-edit"/></button>
              <button type="button"
                      className="btn btn-danger"
                      onClick={this.delete}
                      ><i className="fa fa-trash"/></button>
            </p>
          </div>
        </div>
      </div>
    )
  }
}

export default Thumbnail;