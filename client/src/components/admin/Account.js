import React, { Component } from 'react';
import axios from 'axios';
import Thumbnail from './Thumbnail';
import AccountForm from './AccountForm';
import jwtDecode from 'jwt-decode';
import { Redirect } from 'react-router-dom';

class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errMessage: '',
      isLoaded: false,
      accounts: [],
      curAccount: null,
      isOpenModal: false,
      btnStyle: 'fa fa-plus',
    };
  }

  onToggleAddNewForm = () => {
    this.setState({
      isOpenModal: !this.state.isOpenModal,
      btnStyle: (this.state.btnStyle === 'fa fa-plus') ? 'fa fa-minus' : 'fa fa-plus',
    })
  }

  onAddNew = (data) => {
    // alert(data);
    console.log(data);
    let newItems = [...this.state.accounts, data]
    this.setState({ accounts: newItems });
  }

  onDelete = (index) => {
    let items = [...this.state.accounts];
    let i = items.findIndex(cur => cur.MaTaiKhoan === index);
    items.splice(i, 1);
    this.setState({
      accounts: items
    })
  }

  componentWillMount() {
    let token = JSON.parse(localStorage.getItem('user-token'));
    if (!token) {
      return
    }

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get('http://localhost:3333/api/v1/account')
      .then(res => res.data)
      .then(data => {
        if (data.status > 201) {
          this.setState({
            errMessage: data.message
          })
          return
        }
        this.setState({
          isLoaded: true,
          accounts: data.data
        });
      })
      .catch(err => {
        this.setState({
          isLoaded: true,
          error: err
        })
      })
  }

  // Update 17/05
  onFormClear = () => {
    this.setState({
      curAccount: null,
    })
  }


  getAccount = (index) => {
    let token = JSON.parse(localStorage.getItem('user-token'));
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get(`http://localhost:3333/api/v1/account/${index}`)
      .then(res => res.data)
      .then(data => {
        this.setState({
          curAccount: data.data[0]
        });
      })
      .catch(err => {
        this.setState({
          isLoaded: true,
          errMessage: err
        });
      })

  }

  onUpdateHandler = (index) => {
    this.getAccount(index);

    // Open form
    if (!this.state.isOpenModal)
      this.onToggleAddNewForm();

  }

  onUpdated = (data) => {
    // Copy array
    let arr = this.state.accounts.slice();
    let i = this.state.accounts.findIndex((cur) => cur.Id === data.Id);
    if (i >= 0) {
      arr[i] = data;
      this.setState({
        accounts: arr,
      })
      this.onToggleAddNewForm();
    }
  }

  render() {

    let token = JSON.parse(localStorage.getItem('user-token'));
    if (!token) {
      return <Redirect to='account/signin' />
    }

    let { user, exp } = jwtDecode(token)
    if (user.permissions !== 2) {
      return <Redirect to='/account' />
    }

    let curTime = Date.now() / 1000;
    if (exp < curTime) {
      return <Redirect to="/account/signin" />
    }


    let listThumbnail = this.state.accounts.map((account) => {
      let imgUrl = account.Account_Type_Id === 2 ? "/imgs/account_admin.png" : "/imgs/account_default.png";
      return <Thumbnail key={account.Id}
        index={account.Id}
        name={account.Username}
        description={account.Name}
        onDelete={this.onDelete}
        onUpdate={this.onUpdateHandler}
        image_url={imgUrl}
        type={1}
      />
    })
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="container-fluid">
          <div className="jumbotron">
            <h1>account</h1>
          </div>
          <hr />
          <button type="button"
            className="btn btn-lg btn-success"
            onClick={this.onToggleAddNewForm}
          ><i className={this.state.btnStyle}></i></button>
          <hr />
          <AccountForm isOpenModal={this.state.isOpenModal}
            onAddNew={this.onAddNew}
            onToggleAddNewForm={this.onToggleAddNewForm}
            onFormClear={this.onFormClear}
            onUpdated={this.onUpdated}
            Account={this.state.curAccount}
          />
          <div className="row">
            {listThumbnail}
          </div>
        </div>
      );
    }
  }
}

export default Account;