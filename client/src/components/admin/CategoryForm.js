import React from 'react';
import axios from 'axios';
import './Form.css';

class CategoryForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Category_Name: '',
      isNew: true,
      Category_Id: -1,
      Title: 'Add New Category',
    }
  }

  componentDidUpdate(){
    let Cate = this.props.Category;
    if(Cate !== null && this.state.Category_Id !== Cate.Category_Id)
      this.setState({
        Category_Name: Cate.Category_Name,
        Category_Id: Cate.Category_Id,
        isNew: false,
        Title: 'Edit Category',
      })
  }

  onClear = () => {
    this.props.onFormClear();
    this.setState({
      Category_Name: '',
      Category_Id: -1,
      isNew: true,
      Title: 'Add New Category',
    })
  }

  onSubmit = (event) => {
    event.preventDefault();
    // Add new Event
    if(this.state.isNew === true){
      this.onClear();
      this.props.onToggleAddNewForm();
      let cate = this.state;
      axios.post(`http://localhost:3333/api/v1/category`, cate )
        .then(res => {
          cate = {...cate,Category_Id: res.data.data.insertId};
          this.props.onAddNew(cate);
        })
        .catch(err => {
          console.log(`Error in CREATE API - ${err}`);
        })
    }
    // Edit event
    else{
      let cate = {
        Category_Id: this.state.Category_Id,
        Category_Name: this.state.Category_Name,
      };
      axios.put(`http://localhost:3333/api/v1/category/`+ cate.Category_Id, cate )
        .then(res => {
          this.props.onUpdated(cate);
        })
        .catch(err => {
          console.log(`Error in CREATE API - ${err}`);
        })
    }
    
  }

  onValueChange = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.value;

    this.setState({
      [name]: value
    })
  }

  render() {
    let className = this.props.isOpenModal ? 'panel-success openModel' : 'panel-success closeModel';

    return (

      <div className={className}>
        <div className="panel-heading">
          <h3 className="panel-title">{this.state.Title}</h3>
        </div>
        <div className="panel-body">
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label>Tên Loại Sản Phẩm:</label>
              <input
                type="text"
                className="form-control"
                name="Category_Name"
                value={ this.state.Category_Name }
                onChange={ this.onValueChange }
              />
            </div>
            <br />
            <div className="text-center">
              <button
                type="submit"
                className="btn btn-success btn-lg"
              >Save</button>&nbsp;
                  <button
                type="button"
                className="btn btn-danger btn-lg"
              onClick={ this.onClear }
              >Clear</button>
            </div>
          </form>
        </div>
      </div>

    )
  }
}

export default CategoryForm;