import React from 'react';
import axios from 'axios';
import './Form.css';

class ProductForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Name: '',
      Price: 0,
      InStock: 0,
      Description: '',
      Category_Id: 0,
      Brand_Id: 0,
      Img_URL: '',
      isNew: true,
      Id: -1,
      Title: 'Add New Product',
      ButtonTitle: 'Add',
      lstCategories: [],
      lstBrands: [],
    }
  }

  LoadComboBox = () => {
    // Load list Category
    let token = JSON.parse(localStorage.getItem('user-token'));    
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get("http://localhost:3333/api/v1/category")
    .then(res => res.data)
    .then(data => {
      if (data.status > 201) {
        this.setState({
          errMessage: data.message
        })
        return
      }
      this.setState({
        isLoaded: true,
        lstCategories: data.data,
      });
    })
    .then(() =>{
      this.setState({
        Category_Id: this.state.lstCategories[0].Category_Id,
      })
    })
    .catch(err => {
      this.setState({
        isLoaded: true,
        errMessage: err
      });
    })

    // Load List Brand
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get("http://localhost:3333/api/v1/brand")
    .then(res => res.data)
    .then(data => {
      if (data.status > 201) {
        this.setState({
          errMessage: data.message
        })
        return
      }
      this.setState({
        isLoaded: true,
        lstBrands: data.data,
      });
    })
    .then(() =>{
      this.setState({
        Brand_Id: this.state.lstBrands[0].Brand_Id,
      })
    })
    .catch(err => {
      this.setState({
        isLoaded: true,
        errMessage: err
      });
    });
  }

  componentDidMount(){
    this.LoadComboBox();
  }

  componentDidUpdate(){
    let Product = this.props.Product;
    if(Product !== null && this.state.Id !== Product.Id)
      this.setState({
        Name: Product.Name,
        Price: Product.Price,
        InStock: Product.InStock,
        Description: Product.Description,
        Category_Id: Product.Category_Id,
        Brand_Id: Product.Brand_Id,
        Img_URL: Product.Img_URL,
        isNew: false,
        Id: Product.Id,
        Title: 'Edit Product',
        ButtonTitle: 'Update',
      })
  }


  onClear = () => {
    this.props.onFormClear();
    this.setState({
        Name: '',
        Price: 0,
        InStock: 0,
        Description: '',
        Category_Id: this.state.lstCategories[0].Category_Id,
        Brand_Id: this.state.lstBrands[0].Brand_Id,
        Img_URL: '',
        isNew: true,
        Id: -1,
        Title: 'Add New Product',
        ButtonTitle: 'Add',
    })
  }

  uploadImage = () => {
    return new Promise((resolve, reject) => {
      const form = new FormData(this.form);
      let uploadImage = form.get('uploadImage');
 
      const imgData = new FormData();
      imgData.append('file', uploadImage);
      imgData.append('filename', (uploadImage.name).slice(0, (uploadImage.name).length - 4));

      fetch('http://localhost:3333/api/v1/business/uploadImage', { method: 'POST', body: imgData })
        .then(res => res.json())
        .then(body => {
          resolve(body)
        })
        .catch(err => {
          reject(err);
        })
    })
  }

  onSubmit = (event) => {
    event.preventDefault();

    this.uploadImage()
    .then(body => {
      if(this.state.isNew){
        
        let product = this.state;
        axios.post(`http://localhost:3333/api/v1/product`, product )
          .then(res => {
            console.log(res.data);
            product = {...product,Id: res.data.data.insertId};
            this.props.onAddNew(product);
          })
          .then(() => {
            this.onClear();
            this.props.onToggleAddNewForm();
          })
          .catch(err => {
            console.log(`Error in CREATE API - ${err}`);
          })
      }
      else{
        // Edit
        let item = {
          Id: this.state.Id,
          Name: this.state.Name,
          Price: this.state.Price,
          InStock: this.state.InStock,
          Description: this.state.Description,
          Category_Id: this.state.Category_Id,
          Brand_Id: this.state.Brand_Id,
          Img_URL: this.state.Img_URL,
        };
        let token = JSON.parse(localStorage.getItem('user-token'));
        if (!token) {
          return
        }
    
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        axios.put(`http://localhost:3333/api/v1/product/${item.Id}`, item )
          .then(res => {
            this.props.onUpdated(item);
          })
          .catch(err => {
            console.log(`Error in CREATE API - ${err}`);
          })
      }
    })
  }

  onValueChange = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.value;

    if (name === 'uploadImage') {
      let imageName = value.slice(12, value.length - 3) + 'jpg';
      name = 'Img_URL';
      value = imageName;
    }
    
    this.setState({
      [name]: value
    })
  }

  render() {
    let className = this.props.isOpenModal ? 'panel-success openModel' : 'panel-success closeModel';

    let lstCategories = this.state.lstCategories.map((Category, index) => {
      return <option key={index} value={Category.Category_Id}>{Category.Category_Name}</option>;
    })

    let lstBrand = this.state.lstBrands.map((Brand, index) => {
      return <option key={index} value={Brand.Brand_Id}>{Brand.Brand_Name}</option>;
    })

    return (

      <div className={className} >
        <div className="panel-heading">
          <h3 className="panel-title">{this.state.Title}</h3>
        </div>
        <div className="panel-body">
          <form onSubmit={this.onSubmit} ref={el => (this.form = el)}>
            <div className="form-group">
              <label>Tên Sản Phẩm:</label>
              <input
                type="text"
                className="form-control"
                name="Name"
                value={ this.state.Name }
                onChange={ this.onValueChange }
              />
            </div>
            <div className="form-group">
              <label>Giá Sản Phẩm:</label>
              <input
                type="number"
                className="form-control"
                name="Price"
                value={ this.state.Price }
                onChange={ this.onValueChange }
              />
            </div>
            <div className="form-group">
              <label>Số lượng tồn:</label>
              <input
                type="number"
                className="form-control"
                name="InStock"
                value={ this.state.InStock }
                onChange={ this.onValueChange }
              />
            </div>
            <div className="form-group">
              <label>Mô Tả:</label>
              <input
                type="text"
                className="form-control"
                name="Description"
                value={ this.state.Description }
                onChange={ this.onValueChange }
              />
            </div>
            <div className="form-group">
              <label>Mã Loại Sản Phẩm:</label>
              <select value={this.state.Category_Id} 
                      onChange={this.onValueChange}
                      name="Category_Id"
                      className="form-control">
                {lstCategories}
              </select>
              {/* <input
                type="number"
                className="form-control"
                name="Category_Id"
                value={ this.state.Category_Id }
                onChange={ this.onValueChange }
              /> */}
            </div>
            <div className="form-group">
              <label>Mã Hãng Sản Xuất:</label>
              <select value={this.state.Brand_Id} 
                      onChange={this.onValueChange}
                      name="Brand_Id"
                      className="form-control">
                {lstBrand}
              </select>
            </div>
            <div className="form-group">
              <label>Hình URL:</label>
              <input
                type="text"
                className="form-control"
                name="Img_URL"
                value={ this.state.Img_URL }
                onChange={ this.onValueChange }
              />
            </div>
            <div className="form-group">
              <label>Image:</label>
              <input
                type="file"
                name="uploadImage"
                className="form-control"
                onChange={this.onValueChange}
              />
            </div>
            <br />
            <div className="text-center">
              <button
                type="submit"
                className="btn btn-success btn-lg"
              >{this.state.ButtonTitle}</button>&nbsp;
                  <button
                type="button"
                className="btn btn-danger btn-lg"
              onClick={ this.onClear }
              >Clear</button>
            </div>
          </form>
        </div>
      </div>

    )
  }
}

export default ProductForm;