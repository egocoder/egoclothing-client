import React from 'react';
import axios from 'axios';
import './Admin.css';
import './Form.css';

class AccountForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Username: '',
      Password: '',
      Name: '',
      Address: '',
      PhoneNumber: '',
      Email: '',
      isNew: true,
      Id: -1,
      Title: 'Add New Account',
      ButtonTitle: 'Add',
    }
  }

  componentDidUpdate(){
    let Account = this.props.Account;
    if(Account !== null && this.state.Id !== Account.Id)
      this.setState({
        Username: Account.Username,
        Password: Account.Password,
        Name: Account.Name,
        Address: Account.Address,
        PhoneNumber: Account.PhoneNumber,
        Email: Account.Email,
        Id: Account.Id,
        isNew: false,
        Title: 'Edit Account',
        ButtonTitle: 'Update',
      })
  }

  onClear = () => {
    this.props.onFormClear();
    this.setState({
      Username: '',
      Password: '',
      Name: '',
      Address: '',
      PhoneNumber: '',
      Email: '',
      isNew: true,
      Id: -1,
      Title: 'Add New Category',
      ButtonTitle: 'Add',
    })
  }

  onSubmit = (event) => {
    let token = JSON.parse(localStorage.getItem('user-token'));
    event.preventDefault();
    if(this.state.isNew)
    {
      this.onClear();
      this.props.onToggleAddNewForm();
      let Account = this.state;
      let data = {
        user:{
          ...Account
        }
      }
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
      axios.post(`http://localhost:3333/api/v1/account/register`, data)
        .then(res => {
          Account = {...Account,Id: res.data.data.insertId};
          this.props.onAddNew(Account);
        })
        .catch(err => {
          console.log(`Error in CREATE API - ${err}`);
        })
    }
    else{
      // Edit
      let item = {
        Id: this.state.Id,
        Username: this.state.Username,
        Password: this.state.Password,
        Name: this.state.Name,
        Address: this.state.Address,
        PhoneNumber: this.state.PhoneNumber,
        Email: this.state.Email,
        IsRemoved: 0,
      };
      let token = JSON.parse(localStorage.getItem('user-token'));
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
      axios.put(`http://localhost:3333/api/v1/account/${item.Id}`, item )
        .then(res => {
          this.props.onUpdated(item);
        })
        .catch(err => {
          console.log(`Error in CREATE API - ${err}`);
        })
    }
  }

  onValueChange = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.value;

    this.setState({
      [name]: value
    })
  }

  render() {

    let className = this.props.isOpenModal ? 'panel-success openModel' : 'panel-success closeModel';

    return (
      <div className={className}>
        <div className="panel-heading">
          <h3 className="panel-title">{this.state.Title}</h3>
        </div>
        <div className="panel-body">
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label>Tên Đăng Nhập:</label>
              <input
                required
                type="text"
                className="form-control"
                name="Username"
                value={ this.state.Username }
                onChange={ this.onValueChange }
              />
            </div>
            <div className="form-group">
              <label>Mật Khẩu: </label>
              <input
                required
                type="password"
                className="form-control"
                name="Password"
                value={ this.state.Password }
                onChange={ this.onValueChange }
              />
            </div>
            <div className="form-group">
              <label>Tên Hiển Thị:</label>
              <input
                required
                type="text"
                className="form-control"
                name="Name"
                value={ this.state.Name }
                onChange={ this.onValueChange }
              />
            </div>
            <div className="form-group">
              <label>Địa Chỉ:</label>
              <input
                type="text"
                className="form-control"
                name="Address"
                value={ this.state.Address }
                onChange={ this.onValueChange }
              />
            </div>
            <div className="form-group">
              <label>Điện Thoại:</label>
              <input
                type="text"
                className="form-control"
                name="PhoneNumber"
                value={ this.state.PhoneNumber }
                onChange={ this.onValueChange }
              />
            </div>
            <div className="form-group">
              <label>Email:</label>
              <input
                required
                type="email"
                className="form-control"
                name="Email"
                value={ this.state.Email }
                onChange={ this.onValueChange }
              />
            </div>
            <br />
            <div className="text-center">
              <button
                type="submit"
                className="btn btn-success btn-lg"
              >{this.state.ButtonTitle}</button>&nbsp;
                  <button
                type="button"
                className="btn btn-danger btn-lg"
              onClick={ this.onClear }
              >Clear</button>
            </div>
          </form>
        </div>
      </div>

    )
  }
}

export default AccountForm;