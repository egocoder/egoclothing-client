import React, { Component } from 'react';
import axios from 'axios';
import OrderItem from './OrderItem';
import OrderForm from './OrderForm';

class Order extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      errMessage: '',
      orders: [],
      curOrder: {},
      isOpenModal: false,
    };
  }

  onToggleEditForm = (order_id) => {
    this.getOrder(order_id);
    this.setState({
      isOpenModal: true,
    })
  }

  getOrder = (order_id) => {
    axios.get('http://localhost:3333/api/v1/order/orderid/' + order_id)
      .then(res => res.data)
      .then(data => {
        if (data.status > 201) {
          this.setState({
            errMessage: data.message
          })
          return
        }
        this.setState({
            curOrder: data.data[0],
        });
      })
      .catch(err => {
        this.setState({
          isLoaded: true,
          error: err
        })
      })
  }

  componentWillMount() {
    let token = JSON.parse(localStorage.getItem('user-token'));
    if (!token) {
      return
    }

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get('http://localhost:3333/api/v1/order')
      .then(res => res.data)
      .then(data => {
        if (data.status > 201) {
          this.setState({
            errMessage: data.message
          })
          return
        }
        this.setState({
          isLoaded: true,
          orders: data.data
        });
      })
      .catch(err => {
        this.setState({
          isLoaded: true,
          error: err
        })
      })
  }

  onStatusUpdated = (Status_Id) =>{
    let newOrder = {...this.state.curOrder};
    newOrder.Status_Id = Status_Id;
    this.setState({
      curOrder: newOrder
    })
  }

  render() {
    let renderOrders = this.state.orders.map((order, index) => {
        if (order.Status_Id < 4) {
          return <OrderItem key={index} index={index} onToggleEditForm={this.onToggleEditForm} order={order} />  
        }
        return null
      })
      return(
        <div className="container-fluid">
            <div className="jumbotron">
            <h1>Order</h1>
            </div>
            <hr />
            <OrderForm isOpenModal={this.state.isOpenModal} 
                        order={this.state.curOrder}
                        onStatusUpdated={this.onStatusUpdated}/>
            <div className="row">
                <div className="col-12">
                    <div className="table-responsive">
                        <table className="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Date</th>
                                <th scope="col">Status</th>
                                <th scope="col">Total</th>
                                <th scope="col">Detail</th>
                                <th scope="col" className="text-right"></th>
                            </tr>
                            </thead>
                            <tbody>
                            {renderOrders}
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      );
  }
}

export default Order;