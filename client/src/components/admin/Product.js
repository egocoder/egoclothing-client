import React, { Component } from 'react';
import axios from 'axios';
import Thumbnail from './Thumbnail';
import ProductForm from './ProductForm';
import jwtDecode from 'jwt-decode';
import { Redirect } from 'react-router-dom';

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errMessage: '',
      isLoaded: false,
      products: [],
      curProduct: null,
      isOpenModal: false,
      btnStyle: 'fa fa-plus',
    };
  }



  onToggleAddNewForm = () => {
    this.setState({
      isOpenModal: !this.state.isOpenModal,
      btnStyle: (this.state.btnStyle === 'fa fa-plus') ? 'fa fa-minus' : 'fa fa-plus',
    })
  }

  onAddNew = (data) => {
    // alert(data);
    let newItems = [...this.state.products, data]
    this.setState({ products: newItems });
  }

  onDelete = (index) => {
    let items = [...this.state.products];
    let i = items.findIndex(cur => cur.Id === index);
    items.splice(i, 1);
    this.setState({
      products: items
    })
  }

  componentDidMount() {
    let token = JSON.parse(localStorage.getItem('user-token'));
    if (!token) {
      return
    }

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get("http://localhost:3333/api/v1/product")
      .then(res => res.data)
      .then(data => {
        if (data.status > 201) {
          this.setState({
            errMessage: data.message
          })
          return
        }
        this.setState({
          isLoaded: true,
          products: data.data
        });
      })
      .catch(err => {
        this.setState({
          isLoaded: true,
          errMessage: err
        });
      })
  }

  // Update 17/05
  onFormClear = () => {
    this.setState({
      curProduct: null,
    })
  }


  getProduct = (index) => {
    let token = JSON.parse(localStorage.getItem('user-token'));
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get(`http://localhost:3333/api/v1/product/${index}`)
      .then(res => res.data)
      .then(data => {
        this.setState({
          curProduct: data.data[0]
        });
      })
      .catch(err => {
        this.setState({
          isLoaded: true,
          errMessage: err
        })
      })
  }

  onUpdateHandler = (index) => {
    this.getProduct(index);

    // Open form
    if (!this.state.isOpenModal) {
      this.onToggleAddNewForm();
    }
  }

  onUpdated = (data) => {
    // Copy array
    let arr = this.state.products.slice();
    let i = this.state.products.findIndex((cur) => cur.Id === data.Id);
    if (i >= 0) {
      arr[i] = data;
      this.setState({
        products: arr,
      })
      this.onToggleAddNewForm();
    }
  }


  render() {
    let token = JSON.parse(localStorage.getItem('user-token'));
    if (!token) {
      return <Redirect to='account/signin' />
    }

    let { user, exp } = jwtDecode(token)
    if (user.permissions !== 2) {
      return <Redirect to='/account' />
    }

    let curTime = Date.now() / 1000;
    if (exp < curTime) {
      return <Redirect to="/account/signin" />
    }

    let listThumbnail = this.state.products.map((Product) => {
      return <Thumbnail key={Product.Id}
        index={Product.Id}
        name={Product.Name}
        description={Product.Price}
        image_url={`http://localhost:3333/public/imgs/${Product.Img_URL}`}
        onDelete={this.onDelete}
        onUpdate={this.onUpdateHandler}
        type={2}
      />
    })
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="container-fluid">
          <div className="jumbotron">
            <h1>product</h1>
          </div>
          <hr />
          <button type="button"
            className="btn btn-lg btn-success"
            onClick={this.onToggleAddNewForm}
          ><i className={this.state.btnStyle}></i></button>
          <hr />
          <ProductForm isOpenModal={this.state.isOpenModal}
            onAddNew={this.onAddNew}
            onToggleAddNewForm={this.onToggleAddNewForm}
            onFormClear={this.onFormClear}
            onUpdated={this.onUpdated}
            Product={this.state.curProduct} />
          <div className="row">
            {listThumbnail}
          </div>
        </div>
      );
    }
  }
}

export default Product;