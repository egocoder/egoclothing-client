import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from './Home';
import ProductDetail from '../store/ProductDetail';
import Store from '../store/Store';
import About from './About';
import Admin from '../admin/Admin';
import Account from '../account/Account';
import AccountForm from '../account/AForm';
import Cart from '../cart/Cart';
import NotFound from './NotFound';

class Main extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/about" component={About} />
        <Route exact path="/admin" component={Admin} />
        <Route exact path="/store" component={Store} />
        <Route exact path="/store/newest" component={Store} />
        <Route exact path="/store/feature" component={Store} />
        <Route exact path='/store/detail/:id' component={ProductDetail}/>
        <Route exact path="/cart" component={Cart} />
        <Route exact path="/account" component={Account} />
        <Route exact path="/account/signin" component={AccountForm} />
        <Route exact path="/account/signup" component={AccountForm} />
        <Route exact path="/account/forgot" component={AccountForm} />
        <Route component={NotFound} />
      </Switch>
    )
  }
}

export default Main;