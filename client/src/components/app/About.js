import React from 'react';
import './About.css';
import { Link } from 'react-router-dom';

class About extends React.Component {

  render() {
    return (

      <div className="about-container">
        <div className="at-section">
          <div className="at-section__title">OUR TEAM</div>
        </div>
        <div className="at-grid" data-column="3">
          <div className="at-column">
            <div className="at-user">
              <div className="at-user__avatar"><img src="/imgs/ntk_ava.jpg" alt="About_Image" /></div>
              <div className="at-user__name">NGUYỄN TUẤN KIỆT</div>
              <div className="at-user__title">Web Developer</div>
              <ul className="at-social">
                <li className="at-social__item"><Link to=""><i className="fa fa-facebook"/></Link></li>
                <li className="at-social__item"><Link to=""><i className="fa fa-envelope"/></Link></li>
              </ul>
            </div>
          </div>
          <div className="at-column">
            <div className="at-user">
              <div className="at-user__avatar"><img src="/imgs/lpl_ava.jpg" alt="About_Image" /></div>
              <div className="at-user__name">LÊ PHÁT LỘC</div>
              <div className="at-user__title">.Net Developer</div>
              <ul className="at-social">
                <li className="at-social__item"><Link to=""><i className="fa fa-facebook"/></Link></li>
                <li className="at-social__item"><Link to=""><i className="fa fa-envelope"/></Link></li>
              </ul>
            </div>
          </div>
          <div className="at-column">
            <div className="at-user">
              <div className="at-user__avatar"><img src="/imgs/nnm_ava.jpg" alt="About_Image" /></div>
              <div className="at-user__name">NGUYỄN NHẬT MINH</div>
              <div className="at-user__title">Game Developer</div>
              <ul className="at-social">
                <li className="at-social__item"><Link to=""><i className="fa fa-facebook"/></Link></li>
                <li className="at-social__item"><Link to=""><i className="fa fa-envelope"/></Link></li>
              </ul>
            </div>
          </div>
          <div className="at-column">
            <div className="at-user">
              <div className="at-user__avatar"><img src="/imgs/pvn_ava.png" alt="About_Image" /></div>
              <div className="at-user__name">PHAN VĂN NHƠN</div>
              <div className="at-user__title">.Net Developer</div>
              <ul className="at-social">
                <li className="at-social__item"><Link to=""><i className="fa fa-facebook"/></Link></li>
                <li className="at-social__item"><Link to=""><i className="fa fa-envelope"/></Link></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default About;