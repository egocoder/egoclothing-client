import React from 'react';

class Home extends React.Component {
  render() {
    return (
      <img width="100%" className="center-block" alt="BackGround_Image" src="imgs/background.jpg"/>
    )
  }
}

export default Home;