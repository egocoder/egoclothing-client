import React from 'react'; 


class NotFound extends React.Component {
  
  render() {
    return (
      <div className="container">
        <img src="/imgs/notfound.png" width="100%" alt="NotFound_Image"/>
      </div>
    )
  }
}

export default NotFound;