import React from 'react';
import './Panel.css';

class Panel extends React.Component {

  render() {
    return (
      <div className="outer-wrapper">
        <div className="s-wrap s-type-2">
          <ul className="s-content">
            <li className="s-item s-item-1"></li>
            <li className="s-item s-item-2"></li>
            <li className="s-item s-item-3"></li>
            <li className="s-item s-item-4"></li>
            <li className="s-item s-item-5"></li>
          </ul>
        </div>
      </div>
    )
  }
}

export default Panel;