import React from 'react';
import './ErrorPanel.css';

class ErrorPanel extends React.Component {
  render() {
    return (
      <div className="error-panel container">
        <h1>{this.props.errMessage}</h1>
      </div>
    )
  }
}

export default ErrorPanel;