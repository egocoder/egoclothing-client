import React from 'react';
import './Header.css';
import { Link } from 'react-router-dom';


class Header extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpenBurger: false
    }
  }

  onToggleBurger = () => {
    this.setState({
      isOpenBurger: !this.state.isOpenBurger
    })
  }

  render() {

    let classStyleBurger = this.state.isOpenBurger ? 'fa fa-close' : 'fa fa-bars'
    let classStyleMobList = this.state.isOpenBurger ? 'navigation open-mob-list' : 'navigation close-mob-list'

    let productCart = JSON.parse(localStorage.getItem('product-cart'));
    let classStyleCart = productCart && productCart.length > 0 ? 'cart-notification' : ''

    return (
      <section className={classStyleMobList}>
        <div className="nav-container">
          <div className="brand">
            <a>EGo Clothing</a>
          </div>
          <nav>
            <div className="nav-mobile">
              <i onClick={this.onToggleBurger} id="nav-toggle" className={classStyleBurger}></i>
            </div>
            <ul className="nav-list">
              <li>
                <Link to='/'>Home</Link>
              </li>
              <li>
                <Link to='/store/newest'>Store</Link>
              </li>
              <li>
                <Link to='/about'>About</Link>
              </li>
              <li className={classStyleCart}>
                <Link to='/cart'><i className="fa fa-shopping-cart"></i></Link>
              </li>
              <li>
                <Link to='/account'><i className="fa fa-user"></i></Link>
              </li>
            </ul>
            <ul id="mob-list">
              <li>
                <Link onClick={this.onToggleBurger} to='/'>Home</Link>
              </li>
              <li>
                <Link onClick={this.onToggleBurger} to='/store/newest'>Store</Link>
              </li>
              <li>
                <Link onClick={this.onToggleBurger} to='/about'>About</Link>
              </li>
              <li>
                <Link onClick={this.onToggleBurger} to='/cart'><i className="fa fa-shopping-cart"></i></Link>
              </li>
              <li>
                <Link onClick={this.onToggleBurger} to='/account'><i className="fa fa-user"></i></Link>
              </li>
            </ul>
          </nav>
        </div>
      </section>
    )
  }
}

export default Header;