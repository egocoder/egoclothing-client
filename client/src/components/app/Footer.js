import React from 'react';
import './Footer.css';
import axios from 'axios';

import { Link } from 'react-router-dom';


class Footer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      receiveMailers: ''
    }
  }

  onSubsctibeEmail = (event) => {
    let { receiveMailers } = this.state;
    event.preventDefault();
    let data = {
      receiveMailers,
      content: `<h1 style="text-align: center; text-transform: uppercase;">thanks you for subscribe email!</h1>`,
      subject: '[SUBSCRIBE EMAIL]'
    }
    axios.post('http://localhost:3333/api/v1/business/sendEmail', data)
    .then(res => res.data)
    .then(data => {
      alert("your mail has sent");
      this.setState({
        receiveMailers: ''
      })
    })
    .catch(err => {
      console.log(err);
    })
  }

  onValueChange = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.value;

    this.setState({
      [name]: value
    })
  }

  render() {
    return (
      <React.Fragment>
        <div className="search-text">
          <div className="container">
            <div className="row text-center">

              <div className="form">
                <h4>SUBSCRIBE TO OUR NEWSLETTER</h4>
                <form id="search-form" className="form-search form-horizontal" method="post">
                  <input type="text" name="receiveMailers" value={this.state.receiveMailers} onChange={this.onValueChange}  className="input-search" placeholder="Email Address" />
                  <button onClick={this.onSubsctibeEmail} type="button" className="btn-search">SUBMIT</button>
                </form>
              </div>

            </div>
          </div>
        </div>

        <footer>
          <div className="container">
            <div className="row">

              <div className="col-md-4 col-sm-6 col-xs-12">
                <span className="logo">EGo Clothing</span>
              </div>

              <div className="col-md-4 col-sm-6 col-xs-12">
                <ul className="menu">
                  <span>Menu</span>
                  <li>
                    <Link to='/'>Home</Link>
                  </li>

                  <li>
                    <Link to='/store/newest'>Store</Link>
                  </li>

                  <li>
                    <Link to='/'>About</Link>
                  </li>
                </ul>
              </div>

              <div className="col-md-4 col-sm-6 col-xs-12">
                <ul className="address">
                  <span>Contact</span>
                  <li>
                    <i className="fa fa-phone" aria-hidden="true"></i> (+84) 123456789
                  </li>
                  <li>
                    <i className="fa fa-map-marker" aria-hidden="true"></i> 227 NGUYEN VAN CU, DICS 3, HCMC
                  </li>
                  <li>
                    <i className="fa fa-envelope" aria-hidden="true"></i> contact@egoclothing.com
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </React.Fragment>
    )
  }
}

export default Footer;