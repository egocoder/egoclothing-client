import React, { Component } from 'react';
import AFormTabItem from './AFormTabItem'

class AFormHeader extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      tabs: [
        {
          path: '/account/signin',
          classNotActive: 'tab signin',
          classActive: 'tab signin active',
          tabName: 'Sign In',
          isActive: true
        },
        {
          path: '/account/signup',
          classNotActive: 'tab signup',
          classActive: 'tab signup active',
          tabName: 'Sign Up',
          isActive: false
        },
        {
          path: '/account/forgot',
          classNotActive: 'tab reset',
          classActive: 'tab reset active',
          tabName: 'Forgot Password',
          isActive: false
        }
      ]
    }
  }

  render() {

    let lstTab = this.state.tabs.map((tab, index) => {
      return (
        <AFormTabItem key={index} index={index} tab={tab} />
      )
    })

    return (
      <div className="tabs">
        {lstTab}
      </div>
    );
  }
}

export default AFormHeader;
