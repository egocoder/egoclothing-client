import React from 'react';

class OrderItem extends React.Component {

  onDeleteOrder = (index) => {
    this.props.onDeleteOrder(this.props.index)
  }

  renameStatus = (status) => {
    let rs = '';
    switch (status) {
      case 2:
        rs = "Delivering"
        break;
      case 3:
        rs = "Paid"
        break;
      case 4:
        rs = "Cancel"
        break;
      default:
        rs = "Ordered"
        break;
    }
    return rs;
  }

  render() {
    let { order } = this.props;
    let orderStatus = this.renameStatus(order.Status_Id);
    
    return (
      <tr>
        <td className="cart-item-name">{order.Order_Id}</td>
        <td>{order.Date}</td>
        <td>{orderStatus}</td>
        <td>$ {order.Total}</td>
        <td className="text-right">
          <button onClick={this.onDeleteOrder} className="btn btn-sm btn-danger">
            <i className="fa fa-trash"></i>
          </button>
        </td>
      </tr>
    )
  }
}

export default OrderItem;