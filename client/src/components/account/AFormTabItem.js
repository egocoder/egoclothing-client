import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class AFormTabItem extends Component {

  render() {
    let {pathname} = window.location;
    let { tab } = this.props;
    return (
      <Link className={tab.path === pathname ? tab.classActive : tab.classNotActive} to={tab.path}>{tab.tabName}</Link>
    );
  }
}

export default AFormTabItem;
