import React from 'react';
import axios from 'axios';

class AccountForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            address: '',
            phoneNumber: '',
            email: '',
            account: {},
            oldPassword: '',
            userOldPassword: '',
            newPassword: '',
            rePassword: '',
            canChangePassword: true,
            errorMessage: '',
            isOpenAccountForm: false
        }
    }

    componentDidMount() {
        if (this.props.userId) {
            let id = this.props.userId;
            axios.get(`http://localhost:3333/api/v1/account/${id}`)
                .then(res => res.data.data[0])
                .then(account => {
                    this.setState({
                        account,
                        name: account.Name,
                        address: account.Address,
                        phoneNumber: account.PhoneNumber,
                        email: account.Email,
                        oldPassword: account.Password,
                    })
                });
        }
    }

    Update = () => {
        let tempAccount = this.state.account;
        tempAccount.Name = this.state.name
        tempAccount.Address = this.state.address
        tempAccount.PhoneNumber = this.state.phoneNumber
        tempAccount.Email = this.state.email
        this.setState({
            account: tempAccount
        });
        let account = this.state.account;
        axios.put(`http://localhost:3333/api/v1/account/${account.Id}`, account)
            .then(res => {
                console.log(res);
            })
            .catch(err => {
                console.log(`Error in CREATE API - ${err}`);
            })

    }

    onChangePassword = () => {
        if (this.state.userOldPassword !== this.state.oldPassword) {
            this.setState({
                canChangePassword: false,
                errorMessage: 'Wrong old password!'
            })
            return;
        }
        if (this.state.newPassword !== this.state.rePassword) {
            this.setState({
                canChangePassword: false,
                errorMessage: "Password and RePassword aren't correct!",
            })
            return;
        }
        if (this.state.newPassword === "" || this.state.userOldPassword === "") {
            this.setState({
                canChangePassword: false,
                errorMessage: "Please fill all required password!",
            })
            return;
        }
        else {
            this.setState({
                canChangePassword: true,
            })
            let user = {
                Email: this.state.email,
                Password: this.state.newPassword
            }
            axios.put(`http://localhost:3333/api/v1/account/forgot/password/`, { user })
                .then(res => {
                    alert(`Your password was updated!`);
                    this.setState({
                        userOldPassword: '',
                        newPassword: '',
                        rePassword: ''
                    })
                })
                .catch(err => {
                    console.log(`Error in CREATE API - ${err}`);
                })
        }
    }

    onValueChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;

        this.setState({
            [name]: value
        })
    }

    onToggleAccountForm = () => {
        this.setState({
            isOpenAccountForm: !this.state.isOpenAccountForm
        })
    }

    render() {

        let styleClassAccountForm = this.state.isOpenAccountForm ? 'is-open-account-form' : 'is-close-account-form';

        return (
            <div className="row">
                <div id="account-btn">
                    <button id="btnSignOut" className="btn btn-info" onClick={this.onToggleAccountForm}>MODIFY INFOMATION</button>
                </div>
                <div className={styleClassAccountForm}>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h2>MODIFY INFORMATION</h2>
                        <div className="form-group">
                            <label>Full Name</label>
                            <input type="text"
                                name="name"
                                value={this.state.name}
                                onChange={this.onValueChange}
                                className="form-control" />
                        </div>
                        <div className="form-group">
                            <label>Address</label>
                            <input type="text"
                                name="address"
                                value={this.state.address}
                                onChange={this.onValueChange}
                                className="form-control" />
                        </div>
                        <div className="form-group">
                            <label>Phone Number</label>
                            <input type="text"
                                name="phoneNumber"
                                value={this.state.phoneNumber}
                                onChange={this.onValueChange}
                                className="form-control" />
                        </div>
                        <div className="form-group">
                            <label>Email</label>
                            <input type="text"
                                name="email"
                                value={this.state.email}
                                onChange={this.onValueChange}
                                className="form-control" />
                        </div>
                        <button className="btn btn-primary"
                            onClick={this.Update}>Update</button>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h2>MODIFY PASSWORD</h2>
                        <div className="form-group">
                            <label>Old Password</label>
                            <input type="password"
                                name="userOldPassword"
                                required
                                value={this.state.userOldPassword}
                                onChange={this.onValueChange}
                                className="form-control" />
                        </div>
                        <div className="form-group">
                            <label>New Password</label>
                            <input type="password"
                                name="newPassword"
                                required
                                value={this.state.newPassword}
                                onChange={this.onValueChange}
                                className="form-control" />
                        </div>
                        <div className="form-group">
                            <label>Retype New Password</label>
                            <input type="password"
                                name="rePassword"
                                required
                                value={this.state.rePassword}
                                onChange={this.onValueChange}
                                className="form-control" />
                        </div>
                        {(!this.state.canChangePassword) ? <div className="alert alert-danger">{this.state.errorMessage}</div> : ""}
                        <button className="btn btn-primary"
                            onClick={this.onChangePassword}>Update Password</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default AccountForm;