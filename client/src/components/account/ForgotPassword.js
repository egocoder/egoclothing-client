import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import randomString from 'randomstring';

class ForgotPassword extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      errMessage: '',
      txtEmail: '',
    }
  }

  onHandleChange = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      // change all state = [name]
      [name]: value
    })
  }

  sendEmail = (Email, Password) => {
    let data = {
      receiveMailers: Email,
      content: `<h1>YOUR NEW PASSWORD - ${Password}</h1>`,
      subject: '[RESET PASSWORD]'
    }
    axios.post('http://localhost:3333/api/v1/business/sendEmail', data)
    .then(res => res.data)
    .then(data => {
      alert("your mail has sent");
    })
    .catch(err => {
      console.log(err);
    })
  }

  onHandleSubmit = (event) => {
    
    event.preventDefault();
    let { txtEmail } = this.state;
    
    let Password = randomString.generate({
      length: 12,
      charset: 'alphabetic'
    });

    let user = {
      Email: txtEmail,
      Password
    }

    axios.put('http://localhost:3333/api/v1/account/forgot/password', {user})
    .then(res => res.data)
    .then(data => {
      if (data.status > 201) {
        this.setState({
          errMessage: data.message
        })
        return
      }
      this.sendEmail(user.Email, user.Password);
      this.setState({
        txtEmail: ''
      })
      
    })
    .catch(err => {
      this.setState({
        errMessage: err.message
      })
    })




  }

  render() {

    if (this.state.isLogined) {
      this.props.history.replace('/cart');
      if (this.state.permissions !== 2) {
        return <Redirect to='/account'/>
      }
      return (
        <Redirect to='/admin'/>
      )
    }

    return (
      <div className="signin-cont cont">
        <h1 className="err-message">{this.state.errMessage}</h1>
        <form method="post" onSubmit={ this.onHandleSubmit }>
          <input  type="name"
                  name="txtEmail"
                  id="username"
                  className="inpt"
                  required="required"
                  placeholder="Enter your Email"
                  value={this.state.txtEmail}
                  onChange={ this.onHandleChange }
                  />
          <div className="submit-wrap">
            <input type="submit" value="Submit" className="submit" />
          </div>
        </form>
      </div>
    );
  }
}

export default ForgotPassword;
