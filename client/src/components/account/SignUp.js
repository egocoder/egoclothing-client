import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errMessage: '',
      txtUsername: '',
      txtEmail: '',
      txtPassword: '',
      txtFullname: '',
      txtAddress: '',
      txtPhone: '',
      isRegisted: false
    }
  }

  onHandleChange = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      [name]: value
    })
  }

  onHandleSubmit = (event) => {

    event.preventDefault();
    let { txtUsername, txtEmail, txtPassword, txtFullname, txtAddress, txtPhone } = this.state;

    let user = {
      Username: txtUsername,
      Email: txtEmail,
      Password: txtPassword,
      Name: txtFullname,
      Address: txtAddress,
      PhoneNumber: txtPhone
    }

    axios.post('http://localhost:3333/api/v1/account/register', { user })
      .then(res => res.data)
      .then(data => {
        if (data.status > 201) {
          this.setState({
            errMessage: data.message
          })
          return
        }
        this.setState({
          isRegisted: true
        })
      })
      .catch(err => {
        this.setState({
          errMessage: err
        })
      })
  }


  render() {
    if (this.state.isRegisted) {
      return (
        <Redirect to='/account/signin' />
      )
    }
    return (
      <div className="signup-cont cont">
        <h1 className="err-message">{this.state.errMessage}</h1>
        <form method="post" onSubmit={this.onHandleSubmit}>
          <input type="name"
            name="txtUsername"
            id="txtUsername"
            className="inpt"
            required="required"
            placeholder="Enter username"
            onChange={this.onHandleChange}
          />
          <input type="email" name="email" id="txtEmail" className="inpt" required="required" placeholder="Enter email" />
          <input type="password"
            name="txtPassword"
            id="txtPassword"
            className="inpt"
            required="required"
            placeholder="Enter password"
            onChange={this.onHandleChange}
          />
          <input type="name"
            name="txtFullname"
            id="txtFullname"
            className="inpt"
            required="required"
            placeholder="Enter fullname"
            onChange={this.onHandleChange}
          />
          <input type="name"
            name="txtAddress"
            id="txtAddress"
            className="inpt"
            required="required"
            placeholder="Enter address"
            onChange={this.onHandleChange}
          />
          <input type="name"
            name="txtPhone"
            id="txtPhone"
            className="inpt"
            required="required"
            placeholder="Enter phone"
            onChange={this.onHandleChange}
          />
          <div className="submit-wrap">
            <input type="submit" value="Sign up" className="submit" />
          </div>
        </form>
      </div>
    );
  }
}

export default SignUp;
