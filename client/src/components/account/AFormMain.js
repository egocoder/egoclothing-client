import React from 'react';
import SignIn from './SignIn';
import SignUp from './SignUp';
import ForgotPassword from './ForgotPassword';
import { Switch, Route } from 'react-router-dom';

class AFormMain extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path="/account/signin" component={SignIn} />
        <Route exact path="/account/signup" component={SignUp} />
        <Route exact path="/account/forgot" component={ForgotPassword} />
      </Switch>
    );
  }
}

export default AFormMain;
