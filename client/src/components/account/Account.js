import React from 'react';
import axios from 'axios';
import jwtDecode from 'jwt-decode';
import { Redirect } from 'react-router-dom';
import OrderItem from './OrderItem';
import ErrorPanel from '../app/ErrorPanel';
import './Account.css';
import AccountForm from './AccountForm';

class Account extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errMessage: '',
      orders: [],
      userInfo: {},
      isLogined: false
    }
  }

  componentWillMount() {
    let token = JSON.parse(localStorage.getItem('user-token'));
    if (!token) {
      return
    }
    
    let { user } = jwtDecode(token);

    this.setState({
      isLogined: true
    })

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get(`http://localhost:3333/api/v1/account/${user.id}`)
    .then(res => res.data.data[0])
    .then(userInfo => {
      this.setState({
        userInfo
      })
    })
    

    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.get(`http://localhost:3333/api/v1/order/${user.username}`)
      .then(res => res.data)
      .then(data => {
        if (data.status > 201) {
          this.setState({
            errMessage: data.err
          })
          return
        }
        return data.data
      })
      .then(data => {
        this.setState({
          orders: data,
        })
      })
      .catch(err => {
        this.setState({
          errMessage: err.message
        })
      })
  }

  onDeleteOrder = (index) => {
    let token = JSON.parse(localStorage.getItem('user-token'));
    let { username } = jwtDecode(token).user;
    
    let { orders } = this.state;
    let orderDeleted = orders[index];
    
    orders.splice(index, 1);
    this.setState({
      orders
    })


    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.put(`http://localhost:3333/api/v1/order/delete/${username}`, {
      Order_Id: orderDeleted.Order_Id
     })
    .then(res => {
      console.log(res)
    })
    .catch(err => {
      console.log(err)
    })
  }

  onHandleSignOut = () => {
    this.setState({
      isLogined: false
    })
    localStorage.setItem('user-token', null);
  }
  

  render() {
    
    let token = JSON.parse(localStorage.getItem('user-token'));
    if (!token) {
      return <Redirect to="/account/signin" />
    }

    let {exp, user} = jwtDecode(token);
    let curTime = Date.now() / 1000;
    if ( exp < curTime) {
      return <Redirect to="/account/signin" />
    }

    if (!this.state.isLogined) {
      return <Redirect to="/account/signin" />
    }

    let renderOrders = this.state.orders.map((order, index) => {
      if (order.Status_Id < 4) {
        return <OrderItem key={index} index={index} onDeleteOrder={this.onDeleteOrder} order={order} />  
      }
      return null
      
    })

    if (this.state.errMessage !== '') {
      return <ErrorPanel errMessage={this.state.errMessage} />
    }

    return (

      <div className="container">
        <div id="account-info">
          <div>
            <h1>Account Infomation</h1>
            <h3>Full Name: {this.state.userInfo.Name} </h3>
            <h3>Address: {this.state.userInfo.Address} </h3>
            <h3>Phone: {this.state.userInfo.PhoneNumber} </h3>
          </div>
          <div>
            <button id="btnSignOut" className="btn btn-danger" onClick={this.onHandleSignOut}>SIGN OUT</button>
          </div>
          </div>
        <AccountForm userId={user.id}/>
        <hr/>
        <h2>Your Order</h2>
        <div className="row">
          <div className="col-12">
            <div className="table-responsive">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Total</th>
                    <th scope="col" className="text-right"></th>
                    
                  </tr>
                </thead>
                <tbody>
                  {renderOrders}
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div >
    )
  }
}
export default Account;