import React from 'react';
import './AForm.css';

import AFormHeader from './AFormHeader';
import AFormMain from './AFormMain';


class AccountForm extends React.Component {

  render() {
    return (
      <section className="my-container">
        <article className="half">
          <h1>Account</h1>
          <div className="content">
            <AFormHeader />
            <AFormMain />
          </div>
        </article>/
      </section>
    )
  }
}

export default AccountForm;