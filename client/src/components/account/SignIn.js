import React from 'react';
import axios from 'axios';
import jwtDecode from 'jwt-decode';
import { Redirect } from 'react-router-dom';

class SignIn extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      permissions: -1,
      errMessage: '',
      txtUsername: '',
      txtPassword: '',
      isLogined: false
    }
  }

  onHandleChange = (event) => {
    let target = event.target;
    let name = target.name;
    let value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      // change all state = [name]
      [name]: value
    })
  }

  onHandleSubmit = (event) => {
    
    event.preventDefault();
    let { txtUsername, txtPassword } = this.state;
    
    let user = {
      username: txtUsername,
      password: txtPassword
    }

    axios.post('http://localhost:3333/api/v1/account/login', {user})
    .then(res => res.data)
    .then(data => {
      if (data.status > 201) {
        this.setState({
          errMessage: data.message
        })
        return
      }
      localStorage.setItem('user-token', JSON.stringify(data.token));
      let { permissions } = jwtDecode(data.token).user;
      this.setState({
        isLogined: true,
        permissions
      })
    })
    .catch(err => {
      this.setState({
        errMessage: err.message
      })
    })
    // console.log(this.state);
  }

  render() {

    if (this.state.isLogined) {
      this.props.history.replace('/cart');
      if (this.state.permissions !== 2) {
        return <Redirect to='/account'/>
      }
      return (
        <Redirect to='/admin'/>
      )
    }

    return (
      <div className="signin-cont cont">
        <h1 className="err-message">{this.state.errMessage}</h1>
        <form method="post" onSubmit={ this.onHandleSubmit }>
          <input  type="name"
                  name="txtUsername"
                  id="username"
                  className="inpt"
                  required="required"
                  placeholder="Enter username"
                  onChange={ this.onHandleChange }
                  />
          <input  type="password"
                  name="txtPassword"
                  id="password"
                  className="inpt"
                  required="required"
                  placeholder="Enter password"
                  onChange={ this.onHandleChange }
                  />
          {/* <input type="checkbox" id="remember" className="checkbox" />
          <label>Remember me</label> */}
          <div className="submit-wrap">
            <input type="submit" value="Sign in" className="submit" />
            {/* <a href="#" className="more">Forgot your password?</a> */}
          </div>
        </form>
      </div>
    );
  }
}

export default SignIn;
